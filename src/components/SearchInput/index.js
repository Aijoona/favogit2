import React from 'react';
import PropTypes from 'prop-types';
import LoadingSpinner from './LoadingSpinner';

const SearchInput = ({term, loading, onSubmit}) => (
    <section className="searcher mt-1">
        <div className="container">
            <div className="row justify-content-center">
                <input
                    className="form-control form-control-lg col-11"
                    type="text"
                    placeholder="Search..."
                    defaultValue={term}
                    onKeyUp={e => {
                        if (e.keyCode === 13) {
                            onSubmit(e.target.value)
                        }
                    }}
                />
                {loading ? <LoadingSpinner/> : null}
            </div>
        </div>
    </section>
);

SearchInput.propTypes = {
    term: PropTypes.string,
    onSubmit: PropTypes.func.isRequired
};

export default SearchInput;
