import React from 'react'
import {Offline} from "react-detect-offline";


const getMessage = message => ({
    'Rejected!': 'This request failed as expected. See MockApi#search'
}[message] || 'Something went wrong, please try again later');

const digestErrors = errors => errors
    .map((e) => getMessage(e.toString()))
    .map((e) => (
        <div className="alert alert-danger" role="alert" key={e}>{e}</div>)
    );

export default ({errors = []}) => (
    <div className="ErrorAlertComponent">
        {digestErrors(errors)}

        <Offline>
            <div className="alert alert-danger" role="alert">
                The internet connection appears to be offline
            </div>
        </Offline>
    </div>
);
