context('Favorites', () => {
    describe('starts empty', () => {
        it('pressing enter triggers search', () => {
            cy.visit('/favorites');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
        });
    });

    describe('search without results', () => {
        it('searching somethin', () => {
            cy.visit('/');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
            cy.get('.searcher input').type('some_weird_search_without_results{enter}');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
        });
    });

    describe('favorite sorting', () => {
        it('allows sort favorites by using the sorter select', () => {
            cy.visit('/');
            cy.get('.searcher input').type('thomas{enter}');
            cy.get('.profile-list').find('.profile-preview:eq(0) .favorite').click();
            cy.get('.profile-list').find('.profile-preview:eq(1) .favorite').click();
            cy.get('.profile-list').find('.profile-preview:eq(2) .favorite').click();
            cy.get('.profile-list').find('.profile-preview:eq(3) .favorite').click();

            cy.get('.favorite-menu-clickable').click();
            cy.get('.view-all').click();

            cy.get('.profile-list').find('.profile-preview').should('have.length', 4);

            cy.get('.profile-list').find('.profile-preview:eq(0) .alias').should('contain', '@ThomasBurleson');

            cy.get('.list-sorter select').select('First added');

            cy.get('.profile-list').find('.profile-preview').should('have.length', 4);
            cy.get('.profile-list').find('.profile-preview:eq(3) .alias').should('contain', '@ThomasBurleson');
        });
    });
});
