import { connect } from 'react-redux'
import Header from '../components/Header'

const computeFavoritesForMenu = favorites => favorites
    .sort((a, b) => a.favorited_at > b.favorited_at ? -1 : 1)
    .slice(0, 3);

const mapStateToProps = state => {
    return {
        favorites: computeFavoritesForMenu(state.favorites.profiles)
    }
};

export default connect(
    mapStateToProps
)(Header);
