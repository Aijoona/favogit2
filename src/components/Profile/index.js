import React from 'react'
import Loading from './Loading';
import Error from './Error';
import ProfileContent from './Profile';
import './index.scss';

class Profile extends React.Component {
    componentDidMount() {
        this.props.onComponentDidMount(this.props.login);
    }

    render() {
        return <div className="profile-wrapper">
            {this.createContent()}
        </div>;
    }

    createContent() {
        if (this.props.error) {
            return <Error/>
        }

        if (this.props.loading) {
            return <Loading/>
        }

        return <ProfileContent profile={this.props.profile} onFavoriteClick={this.props.onFavoriteClick}/>;
    }
}

export default Profile;
