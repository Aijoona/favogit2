import {PROFILE_FETCH_ERROR, PROFILE_FETCH_START, PROFILE_FETCH_SUCCESS} from "../actions/type";

const initialState = {
    login: '',
    profile: null,
    loading: true,
    error: 0
};

export default function (state = initialState, action) {
    let payload = action.payload;

    switch (action.type) {
        case PROFILE_FETCH_START:
            return {
                login: payload.login,
                error: false,
                loading: true,
                profile: payload
            };
        case PROFILE_FETCH_SUCCESS:
            return {
                error: false,
                loading: false,
                profile: payload,
                login: payload.login
            };
        case PROFILE_FETCH_ERROR:
            return {
                ...initialState,
                loading: false,
                error: payload
            };
        default:
            return state;
    }
}
