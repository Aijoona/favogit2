import React from 'react'

import LoadingSvg from '../svg/Loading';

import './Loading.scss';

const Loading = () => (
    <div className="profile-loading">
        <LoadingSvg />
    </div>
);

export default Loading;