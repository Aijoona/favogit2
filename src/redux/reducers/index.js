import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import error from './error';
import home from './home';
import profile from './profile';
import favorites from './favorites';

export default (history) => combineReducers({
    router: connectRouter(history),
    home,
    error,
    profile,
    favorites
});
