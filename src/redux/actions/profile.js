import {
    FAVORITE_LIST_CHANGED,
    PROFILE_FETCH_ERROR,
    PROFILE_FETCH_START,
    PROFILE_FETCH_SUCCESS,
    PROFILE_TOGGLE_FAVORITE,
} from './type';

import services from '../../service';

export const fetchProfile = (payload) => function (dispatch) {
    let profile = services.profile(payload);

    profile
        .then(_ => dispatch({type: PROFILE_FETCH_SUCCESS, payload: profile}))
        .catch(error => dispatch({type: PROFILE_FETCH_ERROR, payload: error}));

    dispatch({type: PROFILE_FETCH_START, payload: profile});
};

export const toggleFavorite = login => function (dispatch) {
    let profile = services.favorite(login);

    dispatch({type: PROFILE_TOGGLE_FAVORITE, payload: profile});
    dispatch({type: FAVORITE_LIST_CHANGED, payload: services.favorites()});
};
