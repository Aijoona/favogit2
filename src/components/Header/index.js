import React from 'react'
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom'
import Dropdown from './Dropdown'

import Github from '../svg/Github';

import './index.scss';

const Header = ({favorites}) => (
    <div>
        <header className="fixed-top">
            <div className="navbar navbar-dark bg-dark shadow-sm">
                <div className="container d-flex justify-content-between">
                    <NavLink to="/" className="navbar-brand d-flex align-items-center">
                        <Github/>
                        <strong>FavoGIT</strong>
                    </NavLink>
                    <Dropdown favorites={favorites}/>
                </div>
            </div>
        </header>
    </div>
);

Header.propTypes = {
    favorites: PropTypes.array.isRequired
};

export default Header
