import {FAVORITE_LIST_CHANGED, FAVORITE_SEARCH, FAVORITE_SORT} from "./type";
import services from "../../service";

export const search = payload => ({type: FAVORITE_SEARCH, payload});
export const sort = payload => dispatch => {
    dispatch({type: FAVORITE_SORT, payload});
    dispatch({type: FAVORITE_LIST_CHANGED, payload: services.favorites()});
};
