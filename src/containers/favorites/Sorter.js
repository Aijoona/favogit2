import {connect} from 'react-redux'
import ListSorter from '../../components/ProfileList/Sorter'
import {sort} from '../../redux/actions/favorites';

const mapStateToProps = state => {
    return {}
};

const mapDispatchToProps = dispatch => {
    return {
        onChange: (payload) => dispatch(sort(payload))
    }
};

const FavoritesListSorter = connect(
    mapStateToProps,
    mapDispatchToProps
)(ListSorter);

export default FavoritesListSorter;
