import React from 'react';
import ProfileList from "../containers/favorites/ProfileList";
import SearchInput from "../containers/favorites/SearchInput";
import Sorter from "../containers/favorites/Sorter";

const Favorites = () => (
    <div>
        <SearchInput />
        <Sorter/>
        <ProfileList />
    </div>
);

export default Favorites;
