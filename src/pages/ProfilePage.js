import React from 'react'
import Profile from '../containers/Profile';

const ProfilePage  = ({match}) => (
    <div>
        <Profile login={match.params.login}/>
    </div>
)

export default ProfilePage
