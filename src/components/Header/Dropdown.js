import React from 'react'
import boostrap from 'bootstrap';
import {NavLink} from 'react-router-dom'

import FullHeart from '../svg/FullHeart';

import './Dropdown.scss';

export default ({favorites = []}) => (
    <div className="dropdown">
        <a className="navbar-toggler dropdown-toggle favorite-menu-clickable" href="#" role="button" data-toggle="dropdown">
            <FullHeart/>
        </a>

        <div className="dropdown-menu dropdown-menu-right favorite-menu">
            <h6 className="dropdown-header">Recently favorited</h6>

            {favorites.map((profile) => (
                <NavLink className="dropdown-item" key={profile.id} to={'/profile/' + profile.login}>
                    <img src={profile.avatar_url}/>
                    <strong>@{profile.login}</strong> {profile.name ? `(${profile.name})` : ''}
                </NavLink>))}

            <div className="dropdown-divider"></div>
            <NavLink className="dropdown-item view-all" to="/favorites">View all</NavLink>
        </div>
    </div>
);
