import React from 'react'
import {connect} from 'react-redux';
import ErrorAlert from '../components/ErrorAlert';

const mapStateToProps = state => {
    return {
        errors: state.error
    }
};

export default connect(
    mapStateToProps
)(ErrorAlert);
