import {connect} from 'react-redux'
import {default as ProfileListComponent} from '../../components/ProfileList';
import {toggleFavorite} from "../../redux/actions/profile";
import {
    SORT_BY_FIRST_ADDED,
    SORT_BY_FOLLOWER_COUNT,
    SORT_BY_LAST_ADDED,
    SORT_BY_REPOSITORY_COUNT
} from "../../redux/favoriteSortStrategy";

const search = term => function (profiles) {
    term = term.toLowerCase().trim();

    return profiles.filter(profile =>
        profile.name.toLowerCase().indexOf(term) !== -1 || profile.login.toLowerCase().indexOf(term) !== -1
    );
};

const sort = strategy => function (profiles) {
    return profiles.sort({
        [SORT_BY_LAST_ADDED]: (a, b) => a.favorited_at > b.favorited_at ? -1 : 1,
        [SORT_BY_FIRST_ADDED]: (a, b) => a.favorited_at > b.favorited_at ? 1 : -1,
        [SORT_BY_REPOSITORY_COUNT]: (a, b) => a.public_repos > b.public_repos ? -1 : 1,
        [SORT_BY_FOLLOWER_COUNT]: (a, b) => a.followers > b.followers ? -1 : 1
    }[strategy]);
};

const addIndex = () => (favorites) => favorites.map((record, index) => ({...record, index: index + 1}));

const pipe = (payload, steps) => steps.reduce((payload, step) => step(payload), payload);

const mapStateToProps = (state, ownProps) => {
    let favorites = pipe(state.favorites.profiles, [
        search(state.favorites.term),
        sort(state.favorites.sort),
        addIndex()
    ]);

    return {
        profiles: favorites,
        totalCount: favorites.length
    }
};

const mapDispatchToProps = dispatch => ({
    onFavoriteClick: (login) => dispatch(toggleFavorite(login))
});

const ProfileList = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileListComponent);

export default ProfileList;
