import { applyMiddleware, compose, createStore } from 'redux'
import createRootReducer from "./reducers";
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router'
import thunk from 'redux-thunk';

export const history = createBrowserHistory();

export default function configureStore(initialState = {}) {
    return createStore(
        createRootReducer(history),
        initialState,
        compose(
            applyMiddleware(
                thunk,
                routerMiddleware(history)
            ),
        )
    )
}
