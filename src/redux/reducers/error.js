import {ADD_ERROR} from "../actions/type";

let initialState = [];

function addError(state, newError) {
    return state.concat(newError).reduce(function (memo, error) {
        if (memo.indexOf(error) === -1) {
            memo.push(error);
        }

        return memo;
    }, []);
}

export default function (state = initialState, action) {
    let payload = action.payload;

    switch (action.type) {
        case ADD_ERROR:
            console.error(payload);
            return addError(state, payload);
        default:
            return state;
    }
}