import React from 'react'
import Followers from '../svg/Followers';
import Following from '../svg/Following';
import Repositories from '../svg/Repositories';

import './Profile.scss';

const Profile = ({profile}) => (
    <div className="profile">
        <div className="row justify-content-md-center">
            <div className="col-auto">
                <img src={profile.avatar_url}/>
            </div>
            <div className="col-3 text-left">
                <div className="alias">
                    @{profile.login}
                </div>
                <div className="name">{profile.name}</div>
                <span className="joined">Member since {(new Date(profile.created_at)).toLocaleDateString()}</span>
                <p className="bio">{profile.bio}</p>
            </div>
        </div>
        <div className="row justify-content-md-center">
            <div className="col-5 text-center ">
                <div className="stats">
                    <a className="stat" title="Followers"
                       href={'https://github.com/' + profile.login + '?tab=followers'}
                       target="_blank">
                        <Followers/>
                        <br/>
                        <span className="count">{profile.followers} followers</span>
                    </a>
                    <a className="stat" title="Repositories"
                       href={'https://github.com/' + profile.login + '?tab=repositories'}
                       target="_blank">
                        <Repositories/>
                        <br/>
                        <span className="count">{profile.public_repos} repositories</span>
                    </a>
                    <a className="stat" title="Public gists" href={'https://gist.github.com/' + profile.login}
                       target="_blank">
                        <Repositories/>
                        <br/>
                        <span className="count">{profile.public_gists} gists</span>
                    </a>
                    <a className="stat" title="Following"
                       href={'https://github.com/' + profile.login + '?tab=following'}
                       target="_blank">
                        <Following/>
                        <br/>
                        <span className="count">{profile.following} following</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
);

export default Profile;
