import {
    HOME_PROFILE_FETCH_SUCCESS,
    HOME_SEARCH_ERROR,
    HOME_SEARCH_START,
    HOME_SEARCH_SUCCESS,
    PROFILE_TOGGLE_FAVORITE,
} from "../actions/type";

const initialState = {
    term: '',
    loading: false,
    total_count: 0,
    profiles: []
};

const addIndex = function (profile, index) {
    profile.index = index + 1;

    return profile;
};

const updateProfile = function (profiles, profileToUpdate) {
    return profiles.map(function (profile) {
        return profile.login === profileToUpdate.login
            ? profileToUpdate.toMap()
            : profile;
    })
};

export default function (state = initialState, action) {
    let payload = action.payload;

    switch (action.type) {
        case HOME_SEARCH_START:
            return {
                term: payload,
                loading: true,
                total_count: 0,
                profiles: []
            };
        case HOME_SEARCH_SUCCESS:
            return {
                ...state,
                loading: false,
                total_count: payload.total_count,
                profiles: payload.profiles.map(profile => profile.toMap()).map(addIndex)
            };
        case HOME_SEARCH_ERROR:
            return {
                loading: false,
                total_count: 0,
                profiles: [],
                ...payload
            };
        case PROFILE_TOGGLE_FAVORITE:
        case HOME_PROFILE_FETCH_SUCCESS:
            return {
                ...state,
                profiles: updateProfile(state.profiles, payload).map(addIndex)
            };
        default:
            return state;
    }
}
