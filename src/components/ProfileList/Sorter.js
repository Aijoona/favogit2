import React from 'react'

import {
    SORT_BY_LAST_ADDED,
    SORT_BY_FIRST_ADDED,
    SORT_BY_REPOSITORY_COUNT,
    SORT_BY_FOLLOWER_COUNT
} from "../../redux/favoriteSortStrategy";

import './index.scss';

const Sorter = ({onChange, value = SORT_BY_LAST_ADDED}) => (
    <div className="container-fluid ProfileListSorterComponent">
        <div className="list-sorter form-group row justify-content-end">
            <label
                htmlFor="profile-list-sorter-select"
                className="col-sm-2 col-md-1 col-form-label text-right">
                Order by
            </label>
            <div className="col-sm-10 col-md-2">
                <select className="form-control float-right"
                        id="profile-list-sorter-select"
                        defaultValue={value}
                        onChange={(e) => onChange(e.target.value)}>
                    <option key={SORT_BY_LAST_ADDED} value={SORT_BY_LAST_ADDED}>Last added</option>
                    <option key={SORT_BY_FIRST_ADDED} value={SORT_BY_FIRST_ADDED}>First added</option>
                    <option key={SORT_BY_REPOSITORY_COUNT} value={SORT_BY_REPOSITORY_COUNT}>By repository count</option>
                    <option key={SORT_BY_FOLLOWER_COUNT} value={SORT_BY_FOLLOWER_COUNT}>By followers count</option>
                </select>
            </div>
        </div>
    </div>
);

export default Sorter;
