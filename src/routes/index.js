import React from 'react'
import { Route, Switch } from 'react-router'
import Home from '../pages/Home'
import Favorites from '../pages/Favorites'
import Profile from '../pages/ProfilePage'
import NoMatch from '../pages/NoMatch'
import Header from "../containers/Header";
import ErrorAlert from '../containers/ErrorAlert';
import Footer from "../components/Footer";
import 'bootstrap/dist/css/bootstrap.min.css';

export default (
    <main role="main">
        <Header/>
        <ErrorAlert />
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/favorites" component={Favorites} />
            <Route exact path="/profile/:login" component={Profile} />
            <Route component={NoMatch} />
        </Switch>
        <Footer />
    </main>
)
