## Install

Clone the project

`> git clone git@gitlab.com:Aijoona/favogit2.git`

`> cd favogit2`

Install the dependencies

`> npm install`

## Run

This command launches the application in the port 3000

`npm start`

## Sandbox

The sandbox mode uses a local provider for the API (you can find a bunch of thomas :)

`env REACT_APP_SANDBOX=true npm start`

## Testing

Start the app in the sandbox mode, then launch the test runner (cypress)

`./node_modules/.bin/cypress open`