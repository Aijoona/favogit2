import React from 'react'

const NoMatch = () => (
    <div>
        Not found
    </div>
)

export default NoMatch