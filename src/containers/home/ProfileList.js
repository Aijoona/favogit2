import {connect} from 'react-redux'
import {default as ProfileListComponent} from '../../components/ProfileList';
import {toggleFavorite} from "../../redux/actions/profile";

const mapStateToProps = (state, ownProps) => {
    return {
        profiles: state.home.profiles,
        totalCount: state.home.total_count
    }
};

const mapDispatchToProps = dispatch => ({
    onFavoriteClick: (login) => dispatch(toggleFavorite(login))
});

const ProfileList = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileListComponent);

export default ProfileList;
