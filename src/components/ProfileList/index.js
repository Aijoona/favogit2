import React from 'react';
import ProfileListItem from './ProfileListItem';

import './index.scss';

let profileFactory = function (record, attributes) {
    return <ProfileListItem  {...attributes} />;
};

let profileMapper = (profiles, onFavoriteClick) =>
    profiles.map(profile => profileFactory(profile, {
        profile,
        key: profile.id,
        onFavoriteClick
    }));

const ProfileList = ({profiles, totalCount, onFavoriteClick}) => (
    <div className="profile-list py-5 bg-light">
        <div className="container-fluid">
            <div className="row">
                <div className="col text-right mb-1">Found {totalCount} user(s)</div>
            </div>
            <div className="row">
                <div className="card-columns">
                    {profileMapper(profiles, onFavoriteClick)}
                </div>
            </div>
        </div>
    </div>
);

export default ProfileList;
