import { connect } from 'react-redux'
import {default as SearchInputComponent} from '../../components/SearchInput'
import {search} from "../../redux/actions/home";

const mapStateToProps = state => {
    return {
        term: state.home.term,
        loading: state.home.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: (value) => {
            dispatch(search(value))
        }
    }
};

const SearchInput = connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchInputComponent);

export default SearchInput;
