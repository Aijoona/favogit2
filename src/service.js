import {default as createService} from 'favogit-services';
import MockService from 'favogit-services/test/mock/MockApi';

let overrides;

if (process.env.REACT_APP_SANDBOX) {
    overrides = {api: new MockService()};
}

export default createService(overrides);
