import React from 'react'

import './index.scss';

const Footer = () => (
    <footer className="FooterComponent fixed-bottom">
        <div className="container-fluid">
            <div className="row">
                <div className="col">
                    &copy; Aijoona
                </div>
            </div>
        </div>
    </footer>
);

export default Footer;
