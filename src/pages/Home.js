import React from 'react';
import RemoteSearchInput from '../containers/home/SearchInput';
import HomeProfileList from "../containers/home/ProfileList";

const Home = () => (
    <div>
        <RemoteSearchInput />
        <HomeProfileList />
    </div>
);

export default Home;
