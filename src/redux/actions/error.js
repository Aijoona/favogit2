import {ADD_ERROR} from "./type";

export const addError = (payload) => ({type: ADD_ERROR, payload});