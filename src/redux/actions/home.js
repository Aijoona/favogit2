import {
    HOME_PROFILE_FETCH_SUCCESS,
    HOME_SEARCH_ERROR,
    HOME_SEARCH_START,
    HOME_SEARCH_SUCCESS,
} from "./type";
import services from '../../service';
import {addError} from "./error";


window.services = services;

const searchSuccess = (payload) => function (dispatch) {
    payload.profiles
        .filter(profile => profile.loading())
        .forEach(profile => profile.then(() => dispatch({type: HOME_PROFILE_FETCH_SUCCESS, payload: profile})));

    dispatch({type: HOME_SEARCH_SUCCESS, payload});
};

const searchError = (payload) => function (dispatch) {
    dispatch({type: HOME_SEARCH_ERROR, payload});
    dispatch(addError(payload));
};

export const search = (payload) => function (dispatch) {
    services
        .search(payload)
        .then(result => dispatch(searchSuccess(result)))
        .catch(error => dispatch(searchError(error)));

    return dispatch({type: HOME_SEARCH_START, payload});
};

