context('UI', () => {
    describe('Home', () => {
        it('has header', () => {
            cy.visit('/');
            cy.get('header').contains('FavoGIT');
            cy.get('.dropdown');
        });

        it('has footer', () => {
            cy.visit('/');
            cy.get('footer');
        });

        it('has search input', () => {
            cy.visit('/');
            cy.get('.searcher input');
        });

        it('has search profile list', () => {
            cy.visit('/');
            cy.get('.profile-list');
        });
    });

    describe('Favorites', () => {
        it('has header', () => {
            cy.visit('/favorites');
            cy.get('header').contains('FavoGIT');
            cy.get('.dropdown');
        });

        it('has footer', () => {
            cy.visit('/');
            cy.get('footer');
        });

        it('has search input', () => {
            cy.visit('/');
            cy.get('.searcher input');
        });

        it('has search profile list', () => {
            cy.visit('/');
            cy.get('.profile-list');
        });
    });

    describe('Profile', () => {
        it('has header', () => {
            cy.visit('/profile/Aijoona');
            cy.get('header').contains('FavoGIT');
            cy.get('.dropdown');
        });

        it('has footer', () => {
            cy.visit('/profile/Aijoona');
            cy.get('footer');
        });
    });
});