import {FAVORITE_LIST_CHANGED, FAVORITE_SEARCH, FAVORITE_SORT} from "../actions/type";
import {SORT_BY_LAST_ADDED} from "../favoriteSortStrategy";

let initialState = {
    term: '',
    profiles: [],
    sort: SORT_BY_LAST_ADDED
};

const digestProfiles = profiles => profiles.map(profile => profile.toMap());

export default function (state = initialState, action) {
    let payload = action.payload;

    switch (action.type) {
        case FAVORITE_LIST_CHANGED:
            return {
                ...state,
                profiles: digestProfiles(payload)
            };
        case FAVORITE_SEARCH:
            return {
                ...state,
                term: payload
            };
        case FAVORITE_SORT:
            return {
                ...state,
                sort: payload
            };
        default:
            return state;
    }
}