import React from 'react'
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom'

import Followers from '../svg/Followers';
import Following from '../svg/Following';
import Repositories from '../svg/Repositories';
import FullHeart from "../svg/FullHeart";
import EmptyHeart from "../svg/EmptyHeart";

import './ProfileListItem.scss';

const ProfileListItem = withRouter(({profile, history, onFavoriteClick}) => (
    <div className="card profile-preview shadow-sm" onClick={() => history.push('/profile/' + profile.login)}>
        <span className="index">#{profile.index}</span>
        <img src={profile.avatar_url}/>
        <div className="alias">@{profile.login}</div>
        <div className="name">{profile.name}</div>
        <div className="card-body">
            <p className="bio">{profile.bio}</p>

            <div className="mb-3 text-center ">
                <div className="stats">
                    <div className="stat" title="Followers">
                        <Followers/>
                        <br/>
                        <span className="count">{profile.followers}</span>
                    </div>
                    <div className="stat" title="Repositories">
                        <Repositories/>
                        <br/>
                        <span className="count">{profile.public_repos}</span>
                    </div>
                    <div className="stat" title="Following">
                        <Following/>
                        <br/>
                        <span className="count">{profile.following}</span>
                    </div>
                </div>
            </div>
        </div>
        <div className="footer d-flex justify-content-between align-items-center">
            <div className="registered">
                {profile.created_at ? (new Date(profile.created_at)).toLocaleDateString() : ''}
            </div>
            <div className={'favorite ' + (profile.favorite ? 'favorited' : '')} onClick={(e) => {
                e.stopPropagation();
                onFavoriteClick(profile.login);
            }}>
                {profile.favorite ? <FullHeart/> : <EmptyHeart/>}
            </div>
        </div>
    </div>
));

ProfileListItem.propTypes = {
    profile: PropTypes.object.isRequired,
    onFavoriteClick: PropTypes.func.isRequired,
};

export default ProfileListItem;
