import {connect} from 'react-redux'
import ProfileComponent from '../components/Profile'
import {fetchProfile} from "../redux/actions/profile";

const mapStateToProps = (state, props) => {
    return {
        login: props.login,
        loading: state.profile.loading,
        profile: state.profile.profile,
        error: state.profile.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onComponentDidMount: (login) => dispatch(fetchProfile(login)),
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProfileComponent);
