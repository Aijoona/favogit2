import React from 'react';

export default () => (
    <div className="col">
        <div className="spinner-border mt-1" role="status">
            <span className="sr-only">Loading...</span>
        </div>
    </div>
);