context('Search', () => {
    describe('have results', () => {
        it('pressing enter triggers search', () => {
            cy.visit('/');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
            cy.get('.searcher input').type('thomas{enter}');
            cy.get('.profile-list').find('.profile-preview').should('have.length.greaterThan', 0);
        });
    });

    describe('search without results', () => {
        it('searching somethin', () => {
            cy.visit('/');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
            cy.get('.searcher input').type('some_weird_search_without_results{enter}');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
        });
    });

    describe('searching again...', () => {
        it('modifies the search result', () => {
            cy.visit('/');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
            cy.get('.searcher input').type('thomas{enter}');
            cy.get('.profile-list').find('.profile-preview').its('length').then((size) => {
                cy.get('.searcher input').clear().type('thomasf{enter}');
                cy.get('.profile-list').find('.profile-preview').should('not.have.length', size);
            });
        });

        it('with the same search keeps the same records', () => {
            cy.visit('/');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
            cy.get('.searcher input').type('thomas{enter}');
            cy.get('.profile-list').find('.profile-preview').its('length').then((size) => {
                cy.get('.searcher input').clear().type('thomas{enter}');
                cy.get('.profile-list').find('.profile-preview').should('have.length', size);
            });
        });
    });

    describe('results have...', () => {
        it('user basic information', () => {
            cy.visit('/');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
            cy.get('.searcher input').type('thomasf{enter}');

            cy.get('.profile-list').find('.profile-preview').should('have.length', 1);
            ['alias', 'name', 'registered'].forEach(className => cy.get('.profile-list')
                .find('.profile-preview .' + className)
                .should('have.length', 1)
                .and('not.empty')
            );
        });
    });

    describe('results navigation', () => {
        it('clicking profile item navigates to the profile page', () => {
            cy.visit('/');

            cy.get('.searcher input').type('thomasf{enter}');
            cy.get('.profile-preview').click();

            cy.url().should('include', '/profile/thomasf');
        });
    });

    describe('should handle errors gracefully', () => {
        it('errors clear results and shows an error alert', () => {
            cy.visit('/');

            cy.get('.searcher input').type('thomasf{enter}');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 1);
            cy.get('.ErrorAlertComponent').find('.alert.alert-danger').should('have.length', 0);

            cy.get('.searcher input').clear().type('should_fail{enter}');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 0);
            cy.get('.ErrorAlertComponent').find('.alert.alert-danger')
                .should('have.length', 1)
                .should('contain', 'This request failed as expected. See MockApi#search');
        });
    });

    describe('favoriting results...', () => {
        it('there is a favorite button', () => {
            cy.visit('/');
            cy.get('.searcher input').type('thomasf{enter}');
            cy.get('.profile-list').find('.profile-preview').should('have.length', 1);
            cy.get('.profile-list').find('.profile-preview .favorite').should('have.length', 1);
        });

        it('clicking favorite button changes its state', () => {
            cy.visit('/');
            cy.get('.searcher input').type('thomasf{enter}');

            cy.get('.profile-list').find('.profile-preview .favorite.favorited').should('have.length', 0);
            cy.get('.profile-list').find('.profile-preview .favorite').click();
            cy.get('.profile-list').find('.profile-preview .favorite.favorited').should('have.length', 1);
            cy.get('.profile-list').find('.profile-preview .favorite').click();
            cy.get('.profile-list').find('.profile-preview .favorite.favorited').should('have.length', 0);
        });

        it('favorite state persists between searches', () => {
            cy.visit('/');
            cy.get('.searcher input').type('thomasf{enter}');

            cy.get('.profile-list').find('.profile-preview .favorite').click();
            cy.get('.profile-list').find('.profile-preview .favorite.favorited').should('have.length', 1);

            cy.get('.searcher input').clear().type('thomasf{enter}');
            cy.get('.profile-list').find('.profile-preview .favorite.favorited').should('have.length', 1);
        });

        it('favorited profile appears in the favorite menu', () => {
            cy.visit('/');
            cy.get('.searcher input').type('thomasf{enter}');

            cy.get('.favorite-menu-clickable').click();
            cy.get('.favorite-menu').should('not.contain', 'thomasf');

            cy.get('.profile-list').find('.profile-preview .favorite').click();
            cy.get('.favorite-menu-clickable').click();
            cy.get('.favorite-menu').contains('thomasf');
        });
    });
});