import { connect } from 'react-redux'
import {default as SearchInputComponent} from '../../components/SearchInput'
import {search} from "../../redux/actions/favorites";

const mapStateToProps = state => {
    return {
        term: state.favorites.term,
        loading: state.favorites.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: (value) => {
            dispatch(search(value))
        }
    }
};

const SearchInput = connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchInputComponent);

export default SearchInput;